Python Technical Challenge
==========================
Project carried out to meet the implementation of 2 methods:
1.Return the biggest debtors that still don't have their debts renegotiated.

2.Calculates the minimum number of trips, given a list of requisitions and a value limit.


Installation
------------
.. code-block::
    
    $ make install      


Run tests
------------

To run the tests just run the command:

.. code-block::

    $ make test