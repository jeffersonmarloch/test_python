import pytest

from src.question1 import Contract, Contracts

@pytest.fixture(scope="module")
def contracts():
    return Contracts()

@pytest.fixture(scope="module")
def open_contracts():
    return [Contract(i, i) for i in range(1000)]  # Example: 1000 open contracts

@pytest.fixture(scope="module")
def renegotiated_contracts():
    return [500, 700]  # Example: Renegotiated contract IDs

def test_get_top_N_open_contracts_performance(benchmark, contracts, open_contracts, renegotiated_contracts):
    top_n = 10  # Example: Get the top 10 debtors

    # Measure the execution time of the get_top_N_open_contracts method
    result = benchmark(contracts.get_top_N_open_contracts, open_contracts, renegotiated_contracts, top_n)




@pytest.mark.parametrize(
    "open_contracts, renegotiated_contracts, top_n, expected",
    [
        ([Contract(1,1), Contract(2, 2), Contract(3, 3), Contract(4, 4), Contract(5, 5)], [3], 3, [5, 4, 2]),
        ([Contract(1,1), Contract(2, 2), Contract(3, 3), Contract(4, 4), Contract(5, 5)], [3], 2, [5, 4])
    ],
)
def test_get_top_N_open_contracts(open_contracts, renegotiated_contracts, top_n, expected):
    c = Contracts()
    assert c.get_top_N_open_contracts(open_contracts, renegotiated_contracts, top_n) == expected