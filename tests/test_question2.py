import pytest

from src.question2 import Orders

@pytest.fixture(scope="module")
def orders():
    return Orders()

@pytest.fixture(scope="module")
def requests():
    return [1, 2, 3, 4, 5]  # Example: List of requisitions

def test_combine_orders_performance(benchmark, orders, requests):
    n_max = 5  # Example: Maximum allowed value

    # Measure the execution time of the combine_orders method
    result = benchmark(orders.combine_orders, requests, n_max)

# Parameterizes with the list of requests and n_max and expected number of trips
@pytest.mark.parametrize(
    "requests, n_max, expected",
    [
        ([70, 30, 10], 100, 2),
        ([70, 40, 10], 100, 3),
        ([200, 30, 10], 100, 3)
    ],
)
def test_combine_orders(requests, n_max, expected):
    # Validates the number of trips returned by the method
    assert Orders().combine_orders(requests, n_max,) == expected