class Contract:
    def __init__(self, id, debt):
        self.id = id
        self.debt = debt

    def __str__(self):
        return 'id={}, debt={}'.format(self.id, self.debt)


class Contracts:
    def get_top_N_open_contracts(self, open_contracts, renegotiated_contracts, top_n):
        # Filter open contracts that have not been renegotiated
        open_contracts = [contract for contract in open_contracts if contract.id not in renegotiated_contracts]
        
        # Sort open contracts by debt in descending order
        open_contracts.sort(key=lambda contract: contract.debt, reverse=True)
        
        # Return the N biggest debtors
        return [contract.id for contract in open_contracts[:top_n]]
