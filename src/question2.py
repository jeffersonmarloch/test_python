class Orders:
    def combine_orders(self, requests, n_max):

        item_now = 0  # Index of the first raw element
        next_item = 1  # Index of the next unprocessed element
        num_trips = 0  # Travel counter
        leftover_value = 0 # Value exceeding the maximum value

        while item_now <= len(requests) -1:
            if next_item <= len(requests) -1:
                if requests[item_now] < n_max :
                    if requests[item_now] + requests[next_item] <= n_max:
                        item_now += 2
                        next_item += 2  # Combine the two requests
                        num_trips += 1  # Increment the trip counter
                    
                    else:
                        leftover_value += requests[item_now] + requests[next_item]
                        item_now += 2
                        next_item += 2  # Combine the two requests

                else:
                    if requests[item_now] == n_max:
                        num_trips += 1  # Increment the trip counter
                        item_now += 1
                        next_item += 1  # Combine the two requests
                    
                    else:
                        leftover_value += requests[item_now] - n_max
                        num_trips += 1  # Increment the trip counter
                        item_now += 1
                        next_item += 1  # Combine the two requests
            else:
                if requests[item_now] <= n_max:
                    num_trips += 1  # Increment the trip counter
                    item_now += 1  # Combine the two requests
                    
                else:
                    leftover_value += requests[item_now]
                    item_now += 1  # Combine the two requests

        def get_excess(leftover_value, n_max):
            temp_trips = 0 #Trip counter to take the surplus amount
            while leftover_value > n_max:
                leftover_value -= n_max
                temp_trips += 1
            if leftover_value > 0:
                temp_trips += 1

            return temp_trips
        

        temp = get_excess(leftover_value, n_max)
        num_trips += temp
        
        return num_trips
